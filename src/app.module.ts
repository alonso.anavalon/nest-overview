import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsModule } from './cats/cats.module';
import { TasksController } from './tasks/tasks.controller';

@Module({
  imports: [CatsModule],
  controllers: [AppController, TasksController],
  providers: [AppService],
})
export class AppModule {}
