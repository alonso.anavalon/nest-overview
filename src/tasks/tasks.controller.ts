import { Controller, Get, Post, Body } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';

@Controller('tasks')
export class TasksController {
  @Get()
  getTasks(): string {
    return 'Task';
  }
  @Post()
  createTask(@Body() task: CreateTaskDto): string {
    return task.title;
  }
}
