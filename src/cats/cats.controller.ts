import { Controller, Get, Post, Body, HttpException } from '@nestjs/common';
import { CatsService } from './cats.service';
import { CreateCatDto } from './dto/create-cat.dto';

@Controller('cats')
export class CatsController {
  constructor(private catsService: CatsService) {}

  @Get()
  findAll(): CreateCatDto[] {
    return this.catsService.getCats();
  }

  @Post()
  createCat(@Body() cat: CreateCatDto): void {
    if (cat.gender === false) {
      throw new HttpException('Forbidden', 404);
    }
    this.catsService.createCat(cat);
  }
}
