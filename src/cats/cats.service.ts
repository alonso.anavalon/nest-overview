import { Injectable } from '@nestjs/common';
import { CreateCatDto } from './dto/create-cat.dto';

@Injectable()
export class CatsService {
  private cats: CreateCatDto[] = [];

  createCat(cat: CreateCatDto): string {
    this.cats.push(cat);
    return `Ha agregado el siguiente gato \n ${cat}`;
  }

  getCats(): CreateCatDto[] {
    return this.cats;
  }
}
