export class CreateCatDto {
  name: string;
  age: number;
  gender: boolean;
}
